DokuWiki for Debian
-------------------

You can consult the page <http://www.dokuwiki.org/install:debian> for
more information.

Default password
----------------

During the package installation, you are normally asked for a wiki administrator
name and password. If you have a system on which debconf is insanely configured
to skip even high priority questions, you will not be asked, and default wiki
administrator credentials will be set instead:
* user name: "admin";
* password: "fix-your-debconf-settings".

In such a case, you are advised to change that password immediately by visiting
your wiki <http://localhost/dokuwiki>, and to set sane debconf settings by
running:
    # dpkg-reconfigure debconf

File locations
--------------

DokuWiki's installation directory is /usr/share/dokuwiki/. Some files
and directories were moved and symlinked to comply with the Debian
policy and the FHS:
* data/ is located in /var/lib/dokuwiki/;
* lib/plugins/ and lib/tpl/ are in /var/lib/dokuwiki/;
* conf/ is /etc/dokuwiki/.

In some very specific cases, some user additions to plugins and templates can
get removed from the normal directories and backed up to
/usr/share/dokuwiki/lib/plugins.old or /usr/share/dokuwiki/lib/tpl.old, to be
checked and possibly integrated back to the normal directories
/var/lib/dokuwiki/plugins and /var/lib/dokuwiki/tpl.

Multisite support
-----------------

This package can serve several sites using a single wiki engine. This feature
is called multisite, or farming in DokuWiki vocabulary:
<http://www.dokuwiki.org/tips:farm>.

By default, you only have a single site, available at an address such as
<http://localhost/dokuwiki/>. The default Apache configuration allows you to
serve other sites at addresses such as <http://localhost/dokuwiki/sites/name/>.

To create a new site, you can use the command dokuwiki-addsite(1) with a name
corresponding its address (refer to the man page for more details):
    # dokuwiki-addsite localhost.dokuwiki.sites.name
                       ^^^^^^^^^ ^^^^^^^^^^^^^^^^^^^
                       hostname  path

You may want to define custom aliases or virtual hosts for your multiple sites.
To do so, configure your web server and create your sites with appropriate
names. For instance, for a virtual host-based site at
<http://wiki.project.example.com/>:
    # dokuwiki-addsite wiki.project.example.com
                       ^^^^^^^^^^^^^^^^^^^^^^^^ ^
                       hostname                 null path

Please note that all sites share the installed plugins. For a way to disable
plugins on specific sites, please see
<http://www.dokuwiki.org/tips:farm#enabling_disabling_plugins_in_animals>.

A few tips for Debian's DokuWiki (last update for dokuwiki 2005-02-18a)
--------------------------------

(courtesy from Frédéric Lehobey)

Q: How to handle a local configuration file?
A: Create a file /etc/dokuwiki/local.php where you put all your
preferred values for the configuration variables like:

<?
$conf['superuser'] = '@admin';
?>

(this gives to the 'admin' group the dokuwiki superuser power).  Do
not leave any empty line at the end of this file.

Q: How can I select my language for dokuwiki?
A: Put your language in 'lang' variable like with adding the following
line to /etc/dokuwiki/local.php:

$conf['lang'] = 'fr';

(available languages may by found in /usr/share/dokuwiki/lang).

Q: How to enable ACL?
A: For the 'plain' authentication type, chosen by the (default)

$conf['authtype'] = 'plain';

variable, ACL is enabled by the

$conf['useacl'] = 1;

variable.

This authentication is ruled by the /etc/dokuwiki/acl.auth, and
/etc/dokuwiki/users.auth files.  You have templates of them in
/etc/dokuwiki/ with a .dist suffix appended.

Notice, that if you want to allow users to register themselves, as

$conf['openregister']= 1;

is enabled by default you must have /etc/dokuwiki/users.auth writable
by the web server.  If you are using apache or apache2 with Debian a

# chown www-data /etc/dokuwiki/users.auth

should do the trick.

Notice also clear text emails are sent by dokuwiki containing the
password (used in clear text later anyways).  The dokuwiki email
sender is (optionally) set by the

$conf['mailfrom'] = 'your.email@example.foo';

variable (empty default).  Remember that on a standard Debian
installation, exim4 requires rewriting of the email sender through the
/etc/email-addresses mechanism which would allow dokuwiki issued mail
to go outside only with some

www-data: relevant.email@example.foo

entry in /etc/email-addresses system configuration file.

Finally notice that the (editable) /var/lib/dokuwiki/acl/users.auth.php text file
expects fields in the following order:

user:MD5password:Real Name:email@example.foo:groups,comma,separated

groups being listed _without_ the usual leading '@' for groups.

The default group for new users is chosen by

$conf['defaultgroup']= 'user';

The 'md5sum.textutils --string=password' command (from coreutils
package) may be useful.

Q: What do I have to save when moving (or backuping) dokuwiki
somewhere else?
A: According to the online documentation the files (or directories) to
be saved are:

  /var/lib/dokuwiki/attic/, var/lib/dokuwiki/data,
  /var/lib/dokuwiki/media, and (if any) etc/dokuwiki/local.php

and, for ACL, /var/lib/dokuwiki/acl/acl.auth.php and /var/lib/dokuwiki/acl/users.auth.php
should also be preserved.

In case of inconsistencies, .cache directories in data and media, may
have to be cleared (maybe also your browser ones).

Q: Why won't the new version update the pages?
A: Try removing the files in the cache directory

(rm -r /var/lib/dokuwiki/data/cache/*)

Stylesheet Loading
------------------

It is possible to customise the default stylesheet by creating a CSS
file in /etc/dokuwiki:

 - userstyle.css makes your changes available in screen mode.
 - userprint.css makes your changes available in print mode. 

More information is available at
<https://www.dokuwiki.org/devel:css#user_styles>.
