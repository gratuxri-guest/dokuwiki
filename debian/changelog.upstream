====== Changelog ======


This page contains a summary of changes between the official [[DokuWiki]] releases, only the biggest changes are listed here. The complete and detailed log of all changes is available through the [[http://github.com/splitbrain/dokuwiki/commits/master|GitHub Repository Browser]].

Changelogs of older releases than the ones listed here are available at [[Old Changes]]. Plugin and template developers should read and subscribe to the [[devel:releases|detailed change log]]. 

There is some information regarding the [[install:upgrade|upgrade]]-process.

Changes marked with :!: need some attention on upgrading!

===== Release Candidate 2011-11-10 “Angua RC1” =====

**Usage enhancements**

  * Version handling of media files, just like pages. Old versions are saved and change history available in "recent changes" list as well as in RSS. This and the new media manager are the result of the Google Summer of Code project of Kate Arzamastseva. :!: Be careful when upgrading, some directories need be created (see [[:install:upgrade]])
  * Drag & drop support in media manager to speed up adding new files (only works in Firefox and Chrome). And improved multiple file upload (only works in Firefox, Chrome and Safari). This was made possible by replacing the old flash uploader with [[http://valums.com/ajax-upload/|valums Ajax Upload]].
  * A powerful, AJAX-enhanced [[fullscreen_mediamanager|fullscreen media manager]] is introduced in addition to the standard pop-up manager.  :!: All templates must be updated to support the new manager. :!: Template authors need to add a media manager button and CSS.

**Setup & Administration**

  * Better farming with cascading configuration for plugin enabling/disabling, this was developed as part of the new extension manager Google Summer of Code project of Piyush Mishra. (see [[tips:farm|farm]] & [[config]])
  * Performance enhancement by using data uris. This will transfer small images together with the CSS, thus reducing the number of needed HTTP requests and avoiding the HTTP header overhead. Turn on [[:config:cssdatauri]] for a more efficient install.

**Integration & Development**

  * The core code now uses the [[http://jquery.com|jQuery]] JavaScript framework offering powerful features to plugin & template developers. The old, now deprecated JavaScript API is kept for plugin compatibility. Plugin and template authors are encouraged to migrate their JavaScript code to use jQuery instead of the old API ([[devel:jqueryfaq|jQuery FAQ]]).
  * XML-RPC has been polished fixing some auth & login issues.
  * [[devel:releases|More details about changes]] related to programmers.

**Finally**

  * 25 issues reported in [[http://bugs.dokuwiki.org/]] were fixed.
  * Various smaller bug fixes and enhancements.
  * Thank you Kate and Piyush for their GSoC work and to Google for sponsoring it!

===== Release 2011-05-25a “Rincewind” =====

  * Fixes/changes: 
    * CSS security issue
    * Search/indexer problem with upper or mixed case words on some PHP versions
    * Encoding of non-ASCII mail subjects
    * To allow local links (like ''%%file://c:\docs\myfile.txt%%'') you need to add “file” into your [[urlschemes|conf/scheme.local.conf]]
  * Announcement: http://www.freelists.org/post/dokuwiki/Hotfix-Release-20110525a-Rincewind

===== Release 2011-05-25 “Rincewind” =====

  * :!: The safe method for [[config:fnencode]] [[dokubug>2197|changed]]; be careful when upgrading a wiki with this setting (especially on windows)
  * IPv6 support
  * Inline diff displaying
  * More password hash formats (WordPress, Django)
  * Custom prefixes for [[config:mailprefix|mail subjects]]
  * Support for [[localization#changing_localized_texts_in_your_installation|custom language strings and translations]]
  * Security enhancements for cookie handling
  * Introduced a [[metadata]] index
  * Improvements for handling Asian languages
  * various smaller bug fixes and enhancements
  * :!: A bug in "Rincewind RC1" and recent development versions might have corrupted your [[search#some_background_on_the_searchindex|search index]].\\ It is recommended to check if everything is okay by adding ''?do=check'' to your DokuWiki URL. The index can be rebuilt by using the command line script [[cli#indexerphp|bin/indexer.php]] or the [[plugin:searchindex|Search Index Plugin]].

===== Release 2010-11-07a “Anteater” =====

  * improved quick search algorithm
  * improved email [[subscriptions]]
    * :!: Template authors need to check if they implement the correct button
  * permalinks for diff views
  * license clarification on various 3rd party libs
  * prefilled section names in summary when editing a section
  * license chooser in the installer
  * several IE7, IE8, Chrome fixes
  * Parameters can now be passed in internal links (useful for plugins)
  * some XMLRPC improvements
  * configurable filename encoding
  * support for custom [[devel:section_editor|section editors]] (plugins), for example the [[plugin:edittable|table editor plugin]]
  * centralized library loading
  * lots of bug fixes and minor improvements



===== Release 2009-12-25c “Lemming” =====

  * :!: This release needs PHP 5.1.2 or later!
  * many Editor improvements
    * automatic list indention
    * better headline system
    * link wizard :!: template authors need to provide CSS
  * compatibility fixes for IE8 and Chrome
  * PHP 5.3 compatibility fixes
  * row span support in tables
  * downloadable code blocks
  * fulltext search improvements
  * added filename search in media manager
  * new [[xref>dformat]] function :!: plugin and template authors should replace strftime calls with this function
  * one click revert button for managers :!: template authors need to add this button
  * nicer admin screen :!: template authors need to provide CSS
  * XMLRPC improvements
  * many smaller feature enhancements
  * more plugin events
  * some performance optimizations
  * minor security enhancements
  * many, many, many bug fixes

===== Release 2009-02-14b =====

  * Flash Multiuploader
  * license selector :!: template authors need to make use of [[xref>tpl_license()]]
  * compatibility fixes with Flash Player 10
  * internal changes to make [[devel:farm|farming]] easier
  * removed old upgrade plugins((if you're upgrading from an older version upgrade to intermediate releases first)) :!:
  * better support for non-default auth backends in ACL manager
  * jump to edited section after saving
  * much improved Japanese [[romanization]]
  * improved [[devel:XMLRPC]] interface
  * improved search result display
  * many smaller feature enhancements
  * more plugin events
  * some performance optimizations
  * minor security enhancements
  * security fix for a local file inclusion issue [[bug>1700]] ([[http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1960|CVE-2009-1960]])
  * many, many bug fixes
  

===== Release 2008-05-05 =====

  * Improved RSS [[syndication]], now supports diff views and full HTML, search results are available as RSS
  * Added AJAX to the [[index]] view
  * Show [[diff|diffs]] between arbitrary page revisions
  * Improved search and result highlighting
  * Better plugin support for modifying DokuWiki forms :!: removes *FORM_INJECTION events
  * Separation of [[TOC]] from content for more template flexibility
  * Security measurements against [[wp>CSRF]] attacks
  * X-Sendfile support for supporting Webservers
  * [[XMLRPC]] API
  * Use of [[http://www.aboutus.org/UniversalWikiEditButton|UniversalWikiEditButton]] in default template
  * Complete rewrite of the [[ACL]] manager
  * Moved spell checker from core to plugin
  * Support for deep [[namespace templates]]
  * [[plugin:Popularity]] plugin added by default -- please help us to improve DokuWiki with your data
  * Using locale aware [[phpfn>strftime]] instead of [[phpfn>date]] :!: Plugin authors may need to fix their plugins
  * Use fulltext index for media file usage scan for better scalability
  * Introduction of a temp folder :!: upgraders should make sure ''data/tmp'' exists and is writable
  * Many bugfixes, smaller features and performance improvements.

===== Release 2007-06-26 =====

  * Revert Manager added to mass revert spam attacks
  * OpenSearch Support
  * DOMlib removed (smaller JavaScript footprint) :!:
  * Improved index method for the search :!: Upgraders make sure to have ''data/index'' directory
  * Improved page meta data support
  * Added a manager option to open certain admin functions to a given group
  * Pluggable renderers
  * TOC now can be separated from the content
  * MIME type heuristics to work around IE security flaws
  * include and include_once support for JavaScript files
  * A bucket load of smaller and bigger bugfixes and improvements
  * Some more bugfixes since RC1

===== Release 2006-11-06 =====

  * automatic background [[draft]] saving
  * improved caching behavior for CSS, JavaScript and RSS, with correct IMS handling
  * various performance improvements including the [[devel:changelog]] mechanism redesign
  * support for storing page metadata
  * improvements on the UTF-8 handling when no mb_string extension is available
  * improvements on the default management plugins
  * URL [[rewrite]] support for export formats
  * ATOM 1.0 support for [[syndication]]
  * better feed aggregation with [[http://www.simplepie.org|SimplePie]]
  * [[devel:Action Plugins]]
  * optional gzip output compression if browsers support it
  * namespace restricted [[searchs]] added
  * completely rewritten [[mediamanager]] :!: template designer need to update their templates
  * [[namespace]] links
  * possibility to disable certain DokuWiki actions, when using it in a CMS-like environment
  * made password resetting a two-stage process with confirmation emails
  * webbased install wizard for first time setup
  * many many bug fixes, smaller improvements and language updates

===== Release 2006-03-09 =====

  * Hotfix Release
  * Fixes various bugs ([[bug>736]] [[bug>732]] [[bug>731]] and others)
  * some language updates

===== Release 2006-03-05 =====

  * JavaScript made unobtrusive
  * JavaScript and CSS compression, CSS variable replacements
  * New [[Toolbar]]
  * Fixes for the search indexer [[bug>563]] [[bug>575]], commandline index updater added
  * [[search]] for word parts and other search fixes[[bug>552]] [[bug>632]] [[bug>653]]
  * Completely reworked auth backend system :!: backend config may need to be adjusted
  * stricter XHTML compliance :!: Templates need to be adjusted
  * fixes for the HTTP library [[bug>626]]
  * Google [[sitemap]] generation
  * Admin plugins for managing users, configuration and plugins added
  * Pagelockrefresh through background AJAX
  * simple [[romanization]] support
  * optional hierarchical breadcrumbs added (aka. "You are here")
  * simplified permission setting of files
  * ACL now works with user and groupnames which are not valid pagenames (for some auth backends)
  * XSS security fix for handling EXIF data in the mediamanager

===== Release 2005-09-22 =====

  * various bugs fixed [[bug>550]], [[bug>548]], [[bug>529]], "basedir" problem
  * GeSHi and language updates

===== Release 2005-09-19 =====

  * page template support [[bug>104]]
  * added support for local configuration files [[bug>349]]
  * [[Image]] metadata support (EXIF/IPTC) and detailpage added
  * insitu footnotes
  * removed 2MB limit in fetch.php [[bug>506]]
  * personal wordlist for spellchecker [[bug>488]]
  * [[syndication|feed]] caching
  * email subscription for pagechanges
  * commandline utilities for scripting
  * a new index based [[search]]
    * :!: Template editors need to add the [[http://dev.splitbrain.org/reference/dokuwiki/nav.html?inc/template.php.html#tpl_indexerwebbug|tpl_indexerWebBug()]] function to their main template
    * Upgraders should read [[search#Some Background on the searchindex]]
  * URL rewriting for media files
    * :!: Users of rewrite mode 1 need to adjust their .htaccess
  * [[experimental]] [[plugin:plugin|Plugin Manager]]
  * Support for admin plugins added
  * Proxy support added
  * Optional ImageMagick support added
  * New options for [[image]] inclusion

===== Release 2005-07-13 =====

  * Security fix for [[ACL]] handling [[bug>456]]
  * some fixes for the [[:config:safemodehack]] option [[bug>179]]
  * some spellchecker fixes [[bug>448]]

===== Release 2005-07-01 =====

  * Style fixes
  * TOC Translation Problem fixed [[bug>309]]
  * Various fixes for the getBaseURL autodetection
  * ACL: The password crypting method is configurable through [[config:passcrypt]], defaults to salted MD5
  * ACL: The admin can create new users if openregister is disabled
  * ACL: Users can set their own password ([[config:autopasswd]])
  * ACL: Added DELETE permission for media files :!:
  * fixed problems with footnote numbering
  * Various [[auth:mysql]] changes, support for adding Users :!:
  * Hide IP for logged in users
  * Various [[auth:ldap]] changes :!:
  * Fixed logout [[bug>319]]
  * AJAX pagename [[search]] added
  * Experimental syntax [[Plugin]] support added
  * Fix for <php> mode syntax
  * [[devel:dirlayout|Directory layout]] was changed :!:
  * Experimental Spell Checker added
  * Fix for Sessionlocking [[bug>364]]
  * Removed short_opentag requirement
  * Fixed problem with ZendOptimizer [[bug>377]] [[bug>378]] 
