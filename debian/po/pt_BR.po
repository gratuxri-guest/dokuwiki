# Debconf translations for dokuwiki.
# Copyright (C) 2016 THE dokuwiki'S COPYRIGHT HOLDER
# This file is distributed under the same license as the dokuwiki package.
# Adriano Rafael Gomes <adrianorg@arg.eti.br>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: dokuwiki\n"
"Report-Msgid-Bugs-To: dokuwiki@packages.debian.org\n"
"POT-Creation-Date: 2013-10-27 19:00+0100\n"
"PO-Revision-Date: 2016-01-23 13:22-0200\n"
"Last-Translator: Adriano Rafael Gomes <adrianorg@arg.eti.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "apache2"
msgstr "apache2"

#. Type: multiselect
#. Choices
#: ../templates:1001
msgid "lighttpd"
msgstr "lighttpd"

#. Type: multiselect
#. Description
#: ../templates:1002
msgid "Web server(s) to configure automatically:"
msgstr "Servidor(es) web para configurar automaticamente:"

#. Type: multiselect
#. Description
#: ../templates:1002
msgid ""
"DokuWiki runs on any web server supporting PHP, but only listed web servers "
"can be configured automatically."
msgstr ""
"O DokuWiki executa em qualquer servidor web que tenha suporte a PHP, mas "
"somente os servidores listados podem ser configurados automaticamente."

#. Type: multiselect
#. Description
#: ../templates:1002
msgid ""
"Please select the web server(s) that should be configured automatically for "
"DokuWiki."
msgstr ""
"Por favor, selecione o(s) servidor(es) web que deve(m) ser automaticamente "
"configurado(s) para o DokuWiki."

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Should the web server(s) be restarted now?"
msgstr "O(s) servidor(es) web deve(m) ser reiniciado(s) agora?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"In order to activate the new configuration, the reconfigured web server(s) "
"have to be restarted."
msgstr ""
"Para ativar a nova configuração, o(s) servidor(es) web reconfigurado(s) "
"deve(m) ser reiniciado(s)."

#. Type: string
#. Description
#: ../templates:3001
msgid "Wiki location:"
msgstr "Localização do wiki:"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Specify the directory below the server's document root from which DokuWiki "
"should be accessible."
msgstr ""
"Especifique o diretório abaixo da raiz de documentos do servidor a partir do "
"qual o DokuWiki deve ser acessível."

#. Type: select
#. Description
#: ../templates:4001
msgid "Authorized network:"
msgstr "Rede autorizada:"

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"Wikis normally provide open access to their content, allowing anyone to "
"modify it. Alternatively, access can be restricted by IP address."
msgstr ""
"Wikis normalmente fornecem acesso aberto a seu conteúdo, permitindo que "
"qualquer um o modifique. Alternativamente, o acesso pode ser restringido por "
"endereço IP."

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"If you select \"localhost only\", only people on the local host (the machine "
"the wiki is running on) will be able to connect. \"local network\" will "
"allow people on machines in a local network (which you will need to specify) "
"to talk to the wiki. \"global\" will allow anyone, anywhere, to connect to "
"the wiki."
msgstr ""
"Se você selecionar \"somente localhost\", somente pessoas na máquina local "
"(a máquina na qual o wiki está sendo executado) serão capazes de conectar. "
"\"Rede local\" permitirá que pessoas em máquinas em uma rede local (que você "
"terá que especificar) conversem com o wiki. \"Global\" permitirá qualquer "
"um, de qualquer lugar, se conectar ao wiki."

#. Type: select
#. Description
#: ../templates:4001
msgid ""
"The default is for site security, but more permissive settings should be "
"safe unless you have a particular need for privacy."
msgstr ""
"O padrão é para segurança do site, mas configurações mais permissivas devem "
"ser seguras, a menos que você tenha alguma necessidade específica de "
"privacidade."

#. Type: select
#. Choices
#: ../templates:4002
msgid "localhost only"
msgstr "somente localhost"

#. Type: select
#. Choices
#: ../templates:4002
msgid "local network"
msgstr "rede local"

#. Type: select
#. Choices
#: ../templates:4002
msgid "global"
msgstr "global"

#. Type: string
#. Description
#: ../templates:5001
msgid "Local network:"
msgstr "Rede local:"

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"The specification of your local network should either be an IP network in "
"CIDR format (x.x.x.x/y) or a domain specification (like .example.com)."
msgstr ""
"A especificação da sua rede local deve ser um IP de rede no formato CIDR (x."
"x.x.x/y) ou uma especificação de domínio (como example.com)."

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"Anyone who matches this specification will be given full and complete access "
"to DokuWiki's content."
msgstr ""
"Qualquer um que combinar com essa especificação ganhará total e completo "
"acesso ao conteúdo do DokuWiki."

#. Type: boolean
#. Description
#: ../templates:6001
msgid "Purge pages on package removal?"
msgstr "Expurgar páginas na remoção do pacote?"

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"By default, DokuWiki stores all its pages in a file database in /var/lib/"
"dokuwiki."
msgstr ""
"Por padrão, o DokuWiki armazena todas as suas páginas em um banco de dados "
"de arquivo em /var/lib/dokuwiki."

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"Accepting this option will leave you with a tidier system when the DokuWiki "
"package is removed, but may cause information loss if you have an "
"operational wiki that gets removed."
msgstr ""
"Aceitar essa opção deixará você com um sistema melhor organizado quando o "
"pacote DokuWiki for removido, mas pode causar perda de informação se você "
"tiver um wiki operacional que for removido."

#. Type: boolean
#. Description
#: ../templates:7001
msgid "Make the configuration web-writeable?"
msgstr "Tornar a configuração gravável pela web?"

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"DokuWiki includes a web-based configuration interface. To be usable, it "
"requires the web server to have write permission to the configuration "
"directory."
msgstr ""
"O DokuWiki inclui uma interface de configuração baseada na web. Para ser "
"usável, ela precisa que o servidor web tenha permissão de escrita no "
"diretório de configuração."

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"Accepting this option will give the web server write permissions on the "
"configuration directory and files."
msgstr ""
"Aceitar essa opção dará permissão de escrita ao servidor web no diretório e "
"nos arquivos de configuração."

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"The configuration files will still be readable and editable by hand "
"regardless of whether or not you accept this option."
msgstr ""
"Os arquivos de configuração ainda serão legíveis e editáveis manualmente, "
"independentemente de você aceitar ou não essa opção."

#. Type: boolean
#. Description
#: ../templates:8001
msgid "Make the plugins directory web-writeable?"
msgstr "Tornar o diretório de plugins gravável pela web?"

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"DokuWiki includes a web-based plugin installation interface. To be usable, "
"it requires the web server to have write permission to the plugins directory."
msgstr ""
"O DokuWiki inclui uma interface de instalação de plugins baseada na web. "
"Para ser usável, ela precisa que o servidor web tenha permissão de escrita "
"no diretório de plugins."

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Accepting this option will give the web server write permissions to the "
"plugins directory."
msgstr "Aceitar essa opção dará permissão de escrita no diretório de plugins."

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Plugins can still be installed by hand regardless of whether or not you "
"accept this option."
msgstr ""
"Os plugins ainda poderão ser instalados manualmente, independentemente de "
"você aceitar ou não essa opção."

#. Type: string
#. Description
#: ../templates:9001
msgid "Wiki title:"
msgstr "Título do wiki:"

#. Type: string
#. Description
#: ../templates:9001
msgid ""
"The wiki title will be displayed in the upper right corner of the default "
"template and on the browser window title."
msgstr ""
"O título do wiki será exibido no canto superior direito do modelo padrão e "
"no título da janela do navegador."

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC0 \"No Rights Reserved\""
msgstr "CC0 \"Nenhum Direito Reservado\""

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution"
msgstr "CC Atribuição"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-ShareAlike"
msgstr "CC Atribuição-CompartilhaIgual"

#. Type: select
#. Choices
#: ../templates:10001
msgid "GNU Free Documentation Licence"
msgstr "Licença de Documentação Livre GNU"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-NonCommercial"
msgstr "CC Atribuição-NãoComercial"

#. Type: select
#. Choices
#: ../templates:10001
msgid "CC Attribution-NonCommercial-ShareAlike"
msgstr "CC Atribuição-NãoComercial-CompartilhaIgual"

#. Type: select
#. Description
#: ../templates:10002
msgid "Wiki license:"
msgstr "Licença do wiki:"

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"Please choose the license you want to apply to your wiki content. If none of "
"these licenses suits your needs, you will be able to add your own to the "
"file /etc/dokuwiki/license.php and to refer it in the main configuration "
"file /etc/dokuwiki/local.php when the installation is finished."
msgstr ""
"Por favor, escolha a licença que você quer aplicar ao conteúdo do seu wiki. "
"Se nenhuma dessas licenças satisfizer as suas necessidades, você será capaz "
"de adicionar a sua própria licença ao arquivo /etc/dokuwiki/license.php e de "
"referenciá-la no arquivo de configuração principal /etc/dokuwiki/local.php "
"quando a instalação finalizar."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"Creative Commons \"No Rights Reserved\" is designed to waive as many rights "
"as legally possible."
msgstr ""
"Creative Commons \"Nenhum Direito Reservado\" é projetada para renunciar a "
"todos os direitos legalmente possíveis."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution is a permissive license that only requires licensees to give "
"credit to the author."
msgstr ""
"CC Atribuição é uma licença permissiva que exige apenas que os licenciados "
"deem crédito ao autor."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution-ShareAlike and GNU Free Documentation License are copyleft-"
"based free licenses (requiring modifications to be released under similar "
"terms)."
msgstr ""
"CC Atribuição-CompartilhaIgual e Licença de Documentação Livre GNU são "
"licenças livres baseadas em copyleft (exigindo que as modificações sejam "
"liberadas sob termos similares)."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
"CC Attribution-NonCommercial and CC Attribution-Noncommercial-ShareAlike are "
"non-free licenses, in that they forbid commercial use."
msgstr ""
"CC Atribuição-NãoComercial e CC Atribuição-NãoComercial-CompartilhaIgual são "
"licenças não livres, pois proíbem o uso comercial."

#. Type: boolean
#. Description
#: ../templates:11001
msgid "Enable ACL?"
msgstr "Habilitar ACL?"

#. Type: boolean
#. Description
#: ../templates:11001
msgid ""
"Enable this to use an Access Control List for restricting what the users of "
"your wiki may do."
msgstr ""
"Habilite isso para usar uma Lista de Controle de Acesso para restringir o "
"que os usuários do seu wiki podem fazer."

#. Type: boolean
#. Description
#: ../templates:11001
msgid ""
"This is a recommended setting because without ACL support you will not have "
"access to the administration features of DokuWiki."
msgstr ""
"Essa é uma configuração recomendada porque sem suporte a ACL você não terá "
"acesso às funcionalidades de administração do DokuWiki."

#. Type: string
#. Description
#: ../templates:12001
msgid "Administrator username:"
msgstr "Nome de usuário do administrador:"

#. Type: string
#. Description
#: ../templates:12001
msgid ""
"Please enter a name for the administrator account, which will be able to "
"manage DokuWiki's configuration and create new wiki users. The username "
"should be composed of lowercase ASCII letters only."
msgstr ""
"Por favor, informe um nome para a conta do administrador, o qual será capaz "
"de gerenciar a configuração do DokuWiki e criar novos usuários do wiki. O "
"nome de usuário deve ser composto somente por letras ASCII minúsculas."

#. Type: string
#. Description
#: ../templates:12001
msgid ""
"If this field is left blank, no administrator account will be created now."
msgstr ""
"Se esse campo for deixado em branco, nenhuma conta de administrador será "
"criada agora."

#. Type: string
#. Description
#: ../templates:13001
msgid "Administrator real name:"
msgstr "Nome real do administrador:"

#. Type: string
#. Description
#: ../templates:13001
msgid ""
"Please enter the full name associated with the wiki administrator account. "
"This name will be stored in the wiki password file as an informative field, "
"and will be displayed with the wiki page changes made by the administrator "
"account."
msgstr ""
"Por favor, informe o nome completo associado à conta do administrador do "
"wiki. Esse nome será armazenado no arquivo de senhas do wiki como um campo "
"informativo, e será exibido com as alterações de páginas do wiki feitas pela "
"conta do administrador."

#. Type: string
#. Description
#: ../templates:14001
msgid "Administrator email address:"
msgstr "Endereço de e-mail do administrador:"

#. Type: string
#. Description
#: ../templates:14001
msgid ""
"Please enter the email address associated with the wiki administrator "
"account. This address will be stored in the wiki password file, and may be "
"used to get a new administrator password if you lose the original."
msgstr ""
"Por favor, informe o endereço de e-mail associado à conta do administrador "
"do wiki. Esse endereço será armazenado no arquivo de senhas do wiki, e "
"poderá ser usado para obter uma nova senha do administrador, caso você perca "
"a original."

#. Type: password
#. Description
#: ../templates:15001
msgid "Administrator password:"
msgstr "Senha do administrador:"

#. Type: password
#. Description
#: ../templates:15001
msgid "Please choose a password for the wiki administrator."
msgstr "Por favor, escolha uma senha para o administrador do wiki."

#. Type: password
#. Description
#: ../templates:16001
msgid "Re-enter password to verify:"
msgstr "Informe a senha novamente para verificação:"

#. Type: password
#. Description
#: ../templates:16001
msgid ""
"Please enter the same \"admin\" password again to verify you have typed it "
"correctly."
msgstr ""
"Por favor, informe novamente a mesma senha de \"admin\" para verificar se "
"você digitou-a corretamente."

#. Type: error
#. Description
#: ../templates:17001
msgid "Password input error"
msgstr "Erro ao informar a senha"

#. Type: error
#. Description
#: ../templates:17001
msgid "The two passwords you entered were not the same. Please try again."
msgstr ""
"As duas senhas que você informou não foram as mesmas. Por favor, tente "
"novamente."

#. Type: select
#. Description
#: ../templates:18001
msgid "Initial ACL policy:"
msgstr "Política inicial de ACL:"

#. Type: select
#. Description
#: ../templates:18001
msgid ""
"Please select what initial ACL configuration should be set up to match the "
"intended usage of this wiki:\n"
" \"open\":   both readable and writeable for anonymous users;\n"
" \"public\": readable for anonymous users, writeable for registered users;\n"
" \"closed\": readable and writeable for registered users only."
msgstr ""
"Por favor, selecione qual configuração inicial de ACL deve ser definida para "
"atender o uso pretendido desse wiki:\n"
" \"aberto\":  legível e gravável por usuários anônimos;\n"
" \"público\": legível por usuários anônimos, gravável por usuários "
"registrados;\n"
" \"fechado\": legível e gravável somente por usuários registrados."

#. Type: select
#. Description
#: ../templates:18001
msgid ""
"This is only an initial setup; you will be able to adjust the ACL rules "
"later."
msgstr ""
"Isso é apenas uma configuração inicial; você poderá ajustar as regras de ACL "
"mais tarde."

#. Type: select
#. Choices
#: ../templates:18002
msgid "open"
msgstr "aberto"

#. Type: select
#. Choices
#: ../templates:18002
msgid "public"
msgstr "público"

#. Type: select
#. Choices
#: ../templates:18002
msgid "closed"
msgstr "fechado"
